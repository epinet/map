const $selectLayerInfo = document.getElementById('selectLayerInfo');
const $inputDate = document.getElementById('inputDate');
const $btnDrawer = document.getElementById('btnDrawer');
const $controller = document.getElementById('controller');
const $selectOpacity = document.getElementById('selectOpacity');
const $selectBaseLayer = document.getElementById('selectBaseLayer');
const $topInfoDiv = document.getElementById('topInfoDiv');
const $btnSave = document.getElementById('btnSave');

const mapStatusList = JSON.parse(localStorage.getItem('mapStatusList')) || [];

const vworldLayer = {
    'road': new ol.layer.Tile({
        source: new ol.source.XYZ({
            url: 'https://xdworld.vworld.kr/2d/Base/service/{z}/{x}/{y}.png',
            crossOrigin: 'anonymous',
        })
    }),
    'hybrid': new ol.layer.Tile({
        source: new ol.source.XYZ({
            url: 'https://xdworld.vworld.kr/2d/Hybrid/service/{z}/{x}/{y}.png',
            crossOrigin: 'anonymous',
        })
    }),
    'satellite': new ol.layer.Tile({
        source: new ol.source.XYZ({
            url: 'https://xdworld.vworld.kr/2d/Satellite/service/{z}/{x}/{y}.jpeg',
            crossOrigin: 'anonymous',
        })
    })
};

const basemapLayers = new ol.layer.Group({
    layers: [
        new ol.layer.Group({
            name: 'road',
            layers: [
                vworldLayer['road'],
            ],
            visible: true,
        }),
        new ol.layer.Group({
            name: 'satellite',
            layers: [
                vworldLayer['satellite'],
                vworldLayer['hybrid'],
            ],
            visible: false,
        })
    ]
});

const dataLayer = new ol.layer.Tile({
    source: new ol.source.TileWMS({
        url: null,
        params: null
    }),
    opacity: 0.5
});

const parcelLayer = new ol.layer.Vector({
    source: null,
    style: new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: 'rgba(255, 0, 255, 0.8)',
            width: 1,
        }),
    }),
});

const olLayers = [
    basemapLayers,
    dataLayer,
    parcelLayer,
]

const olMap = new ol.Map({
    layers: olLayers,
    view: new ol.View({
        center: ol.proj.fromLonLat([128, 36]),
        zoom: 7,
        minZoom: 6,
        maxZoom: 19,
        constrainResolution: true,
    }),
    interactions: ol.interaction.defaults({
        // pinchZoom: false,
        altShiftDragRotate: false,
        pinchRotate: false
    }),
    controls: [
        new ol.control.ScaleLine(),
    ],
    target: 'map',
});

$inputDate.valueAsDate = new Date(2020, 11, 1);
$selectOpacity.value = 0.5;

updateDataLayer();

function getSelectedLayerInfo() {
    const s = $selectLayerInfo.value.split('/');
    return {
        path: s.slice(0, s.length - 2).join('/'),
        category: s[s.length - 2],
        layer: s[s.length - 1],
    }
}

function getSelectedDate() {
    return $inputDate.valueAsDate;
}

function getOpacity() {
    return parseFloat($selectOpacity.value);
}

function getDataPath(path, category, layer, date) {
    let m = moment(date);
    let date_dn = m.format('YYYY/MM/DD')
    let date_fn = m.format('YYYY-MM-DD')
    return `${path}/${category}/${layer}/${date_dn}/${layer}.${date_fn}.tiff`;
}

function getMapfilePath(layer) {
    const mapfile_dir = '/srv/webapp/resources/mapfiles';
    return `${mapfile_dir}/${layer}.map`;
}

function changeDataLayer(mapserver_url, mapfile_path, datafile_path, layers) {
    mapserver_url = mapserver_url || 'https://map.epinet.kr/mapserver/';
    mapfile_path = mapfile_path || '/srv/webapp/resources/mapfiles/tmax.map';
    datafile_path = datafile_path || '/data1/coverages/korea_estimation/tmax/1981/01/01/tmax.1981-01-01.tiff';
    layers = layers || 'default';

    dataLayer.setSource(
        new ol.source.TileWMS({
            url: mapserver_url,
            params: {
                map: mapfile_path,
                data: datafile_path,
                layers: layers,
            },
        })
    );
}

function updateDataLayer() {
    const layerInfo = getSelectedLayerInfo();
    const date = getSelectedDate();

    const dataPath = getDataPath(layerInfo.path, layerInfo.category, layerInfo.layer, date);
    const mapfilePath = getMapfilePath(layerInfo.layer);

    changeDataLayer(null, mapfilePath, dataPath);
}

function setParcel(parcel, zoom) {
    console.log(parcel);
    const center = ol.proj.transform([parcel.lon, parcel.lat], 'EPSG:4326', 'EPSG:3857');
    olMap.getView().setCenter(center);
    if (zoom) {
        olMap.getView().setZoom(zoom);
    }

    setParcelLayerByCode(parcel.code);
}

function setParcelLayerByCode(pnuCode) {
    const attrFilter = `pnu:=:${pnuCode}`;
    setParcelLayer(attrFilter, null);
}

function setParcelLayerByCoord(coord) {
    let geomFilter = null;
    if (coord) {
        const [lon, lat] = coord;
        geomFilter = `POINT(${lon} ${lat})`;
    }
    setParcelLayer(null, geomFilter);
}

function setParcelLayerVisibility(visible) {
    parcelLayer.setVisible(visible);
}

function setParcelLayer(attrFilter, geomFilter) {
    parcelLayer.setSource(null);

    if (!attrFilter && !geomFilter) {
        return
    }

    attrFilter = attrFilter || '';
    geomFilter = geomFilter || '';

    const url = 'https://api.vworld.kr/req/data';
    const key = 'AF9CD299-B057-3699-B38C-9C3C007947F2';
    const domain = 'https://map.epinet.kr';
    const data = `service=data&request=GetFeature&crs=EPSG:3857&data=LP_PA_CBND_BUBUN&key=${key}&domain=${domain}&attrFilter=${attrFilter}&geomFilter=${geomFilter}&format=json`;

    $topInfoDiv.innerText = '';

    $.ajax({
        url: url,
        data: data,
        dataType: 'jsonp',
        async: false,
        success: function (data) {
            console.log(data);
            const result = data.response.result;
            let layerSource = null;
            if (result) {
                layerSource = new ol.source.Vector({
                    features: (new ol.format.GeoJSON()).readFeatures(result.featureCollection)
                });

                $topInfoDiv.innerText = result.featureCollection.features[0].properties.addr;
            }
            parcelLayer.setSource(layerSource);
        }
    })
}

function saveStatus() {
    const coord = olMap.getView().getCenter();
    const zoom = olMap.getView().getZoom();
    const layerInfo = getSelectedLayerInfo();
    const date = getSelectedDate();
    const opacity = getOpacity();
    const address = $topInfoDiv.innerText;

    drawMap(60, 60, 1, 1, function (imgData) {
        mapStatusList.push({
            'coord': coord,
            'zoom': zoom,
            'layerInfo': layerInfo,
            'date': date,
            'opacity': opacity,
            'addr': address,
            'thumbnail': imgData,
        });

        localStorage.setItem('mapStatusList', JSON.stringify(mapStatusList));
        refreshMapStatusList();
    });
}

function toggleDrawer() {
    if ($controller.classList.contains('visible')) {
        $btnDrawer.innerHTML = '&#9776;';
    } else {
        $btnDrawer.innerHTML = '&#x2715;';
    }
    $controller.classList.toggle('visible');
    $btnDrawer.classList.toggle('open');
}

function toggleFavorite() {
    $favorite.classList.toggle('visible');
}

function setParcel(parcel) {
    console.log(parcel);
    const center = ol.proj.transform([parcel.lon, parcel.lat], 'EPSG:4326', 'EPSG:3857');
    olMap.getView().setCenter(center);
    olMap.getView().setZoom(17);
}

function testFunc() {
    alert(new Date());
}

$btnDrawer.onclick = (e) => {
    toggleDrawer();
}

$inputDate.onchange = function () {
    updateDataLayer();
}

$selectLayerInfo.onchange = function () {
    updateDataLayer();
}

$selectOpacity.onchange = function () {
    dataLayer.setOpacity(getOpacity());
}

function postMessage() {
    if (window.JavascriptChannel) {
        window.JavascriptChannel.postMessage(JSON.stringify(new Date()));
    }
}

$selectBaseLayer.onchange = function () {
    const layerName = $selectBaseLayer.value;

    basemapLayers.getLayers().forEach((x) => {
        x.setVisible((x.get('name') === layerName));
    });


    localStorage.setItem('mapStatusList', JSON.stringify(mapStatusList));
    refreshMapStatusList();
}

$btnSave.onclick = saveStatus;

function changeMapStatus(index) {
    const favorite = mapStatusList[index];
    console.log(favorite);
    $selectLayerInfo.value = `${favorite.layerInfo.category}/${favorite.layerInfo.layer}`;
    $inputDate.value = moment(favorite.date).format('YYYY-MM-DD');
    $selectOpacity.value = favorite.opacity;

    olMap.getView().setZoom(favorite.zoom);
    olMap.getView().setCenter(favorite.coord);
}

function delMapStatus(index) {
    mapStatusList.splice(index, 1);
    localStorage.setItem('mapStatusList', JSON.stringify(mapStatusList));
    refreshMapStatusList();

}

olMap.on('moveend', function (e) {
    const coord = olMap.getView().getCenter();
    const zoom = olMap.getView().getZoom();

    setParcelLayerByCoord(coord);
});

$favoriteList = document.getElementById('favoriteList');

function refreshMapStatusList() {
    $favoriteList.innerHTML = '';
    mapStatusList.forEach((x, index) => {
        let partialAddr = '';

        if (x.addr) {
            const splitedAddr = x.addr.split(' ');
            partialAddr = splitedAddr.slice(splitedAddr.length - 3).join(' ');
        }

        const html = `<div class="item">
                <div class="del" onclick="delMapStatus(${index})">
                    X
                </div>
                <div class="favoriteItem nb" onclick="changeMapStatus(${index})">
                    <img src="${x.thumbnail}">
                </div>
                <div class="addr">
                    ${partialAddr}
                </div>
            </div>`;
        $favoriteList.innerHTML += html;
    });
}

function drawMap(width, height, multiX, multiY, callback) {
    olMap.once('rendercomplete', function () {
        const [originWidth, originHeight] = olMap.getSize();
        const resizedWidth = width;
        const resizedHeight = height || (originHeight * (resizedWidth / originWidth));

        const mapCanvas = document.createElement('canvas');
        mapCanvas.width = resizedWidth;
        mapCanvas.height = resizedHeight;
        const mapContext = mapCanvas.getContext('2d');
        Array.prototype.forEach.call(
            olMap.getViewport().querySelectorAll('.ol-layer canvas, canvas.ol-layer'),
            function (canvas) {
                if (canvas.width > 0) {
                    const opacity =
                        canvas.parentNode.style.opacity || canvas.style.opacity;
                    mapContext.globalAlpha = opacity === '' ? 1 : Number(opacity);
                    let matrix;
                    const transform = canvas.style.transform;
                    if (transform) {
                        // Get the transform parameters from the style's transform matrix
                        matrix = transform
                            .match(/^matrix\(([^\(]*)\)$/)[1]
                            .split(',')
                            .map(Number);
                    } else {
                        matrix = [
                            parseFloat(canvas.style.width) / canvas.width,
                            0,
                            0,
                            parseFloat(canvas.style.height) / canvas.height,
                            0,
                            0,
                        ];
                    }
                    // Apply the transform to the export map context
                    CanvasRenderingContext2D.prototype.setTransform.apply(
                        mapContext,
                        matrix
                    );

                    const w = width * multiX;
                    const h = height * multiY;
                    const halfWidth = w / 2;
                    const halfHeight = h / 2;
                    const centerX = originWidth / 2;
                    const centerY = originHeight / 2;
                    const x = centerX - halfWidth;
                    const y = centerY - halfHeight;

                    mapContext.drawImage(canvas, x, y, w, h, 0, 0, resizedWidth, resizedHeight);
                }
            }
        );
        /*
        if (navigator.msSaveBlob) {
            // link download attribute does not work on MS browsers
            navigator.msSaveBlob(mapCanvas.msToBlob(), 'map.png');
        } else {
            const link = document.getElementById('image-download');
            link.href = mapCanvas.toDataURL();
            link.click();
        }
        */
        callback(mapCanvas.toDataURL());
    });
    olMap.renderSync();
}

refreshMapStatusList();

const $favoriteCloseBtn = document.getElementById('favoriteCloseBtn');
const $favorite = document.getElementById('favorite');

$favoriteCloseBtn.onclick = function () {
    $favorite.classList.toggle('visible');
}
