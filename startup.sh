#!/usr/bin/env bash

set -o xtrace

# Linux 패키지 설치.
apt install -y \
  nginx \
  fcgiwrap \
  supervisor \
  gdal-bin \
  libgdal-dev \
  cgi-mapserver \
  python3-pip \
  python3-venv


# res 파일 복제.
rsync -arv ./res/ /

# Python 가상환경 생성.
python3 -m venv /srv/venv/main/

# Python 가상환경 활성화.
source /srv/venv/main/bin/activate

# 작업 디렉토리로 이동.
cd /srv/webapp/ || exit

# supervisor 재시작.
supervisorctl update
supervisorctl reload

nginx -s reload
