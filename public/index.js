const $selectLayerInfo = document.getElementById('selectLayerInfo');
const $inputDate = document.getElementById('inputDate');
const $btnDrawer = document.getElementById('btnDrawer');
const $controller = document.getElementById('controller');
const $selectOpacity = document.getElementById('selectOpacity');
const $btnTest = document.getElementById('btnTest');

const vworldLayer = {
    'road': new ol.layer.Tile({
        source: new ol.source.XYZ({
            url: 'https://xdworld.vworld.kr/2d/Base/service/{z}/{x}/{y}.png'
        })
    }),
    'hybrid': new ol.layer.Tile({
        source: new ol.source.XYZ({
            url: 'https://xdworld.vworld.kr/2d/Hybrid/service/{z}/{x}/{y}.png'
        })
    }),
    'satellite': new ol.layer.Tile({
        source: new ol.source.XYZ({
            url: 'https://xdworld.vworld.kr/2d/Satellite/service/{z}/{x}/{y}.jpeg'
        })
    })
};

const basemapLayers = new ol.layer.Group({
    layers: [
        new ol.layer.Group({
            name: 'road',
            layers: [
                vworldLayer['road'],
            ],
            visible: true,
        }),
        new ol.layer.Group({
            name: 'satellite',
            layers: [
                vworldLayer['satellite'],
                vworldLayer['hybrid'],
            ],
            visible: false,
        })
    ]
});

const dataLayer = new ol.layer.Tile({
    source: new ol.source.TileWMS({
        url: null,
        params: null
    }),
    opacity: 0.5
});

const parcelLayer = new ol.layer.Vector({
    source: null,
    style: new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: 'rgba(255, 0, 255, 0.8)',
            width: 1,
        }),
    }),
});

const olLayers = [
    basemapLayers,
    dataLayer,
    parcelLayer,
]

const olMap = new ol.Map({
    layers: olLayers,
    view: new ol.View({
        center: ol.proj.fromLonLat([128, 36]),
        zoom: 7,
        constrainResolution: true,
    }),
    interactions: ol.interaction.defaults({
        // pinchZoom: false,
        altShiftDragRotate: false,
        pinchRotate: false
    }),
    controls: [
        new ol.control.ScaleLine(),
    ],
    target: 'map',
});

$inputDate.valueAsDate = new Date(2020, 11, 1);
$selectOpacity.value = 0.5;

updateDataLayer();

function getSelectedLayerInfo() {
    let [category, layer] = $selectLayerInfo.value.split('/');
    return {
        category: category,
        layer: layer,
    };
}

function getSelectedDate() {
    return $inputDate.valueAsDate;
}

function getOpacity() {
    return parseFloat($selectOpacity.value);
}

function getDataPath(region, workspace, layer, date) {
    let m = moment(date);
    let date_dn = m.format('YYYY/MM/DD')
    let date_fn = m.format('YYYY-MM-DD')
    return `/data1/coverages/${region}_${workspace}/${layer}/${date_dn}/${layer}.${date_fn}.tiff`;
}

function getMapfilePath(layer) {
    const mapfile_dir = '/srv/webapp/resources/mapfiles';
    return `${mapfile_dir}/${layer}.map`;
}

function changeDataLayer(mapserver_url, mapfile_path, datafile_path, layers) {
    mapserver_url = mapserver_url || 'https://tea21.epinet.kr/mapserver/';
    mapfile_path = mapfile_path || '/srv/webapp/resources/mapfiles/tmax.map';
    datafile_path = datafile_path || '/data1/coverages/korea_estimation/tmax/1981/01/01/tmax.1981-01-01.tiff';
    layers = layers || 'default';

    dataLayer.setSource(
        new ol.source.TileWMS({
            url: mapserver_url,
            params: {
                map: mapfile_path,
                data: datafile_path,
                layers: layers,

            },
        })
    );
}

function updateDataLayer() {
    const layerInfo = getSelectedLayerInfo();
    const date = getSelectedDate();

    const dataPath = getDataPath('korea', layerInfo.category, layerInfo.layer, date);
    const mapfilePath = getMapfilePath(layerInfo.layer);

    changeDataLayer(null, mapfilePath, dataPath);
}

function setParcel(parcel, zoom) {
    console.log(parcel);
    const center = ol.proj.transform([parcel.lon, parcel.lat], 'EPSG:4326', 'EPSG:3857');
    olMap.getView().setCenter(center);
    if (zoom) {
        olMap.getView().setZoom(zoom);
    }

    setParcelLayerByCode(parcel.code);
}

function setParcelLayerByCode(pnuCode) {
    const attrFilter = `pnu:=:${pnuCode}`;
    setParcelLayer(attrFilter, null);
}

function setParcelLayerByCoord(coord) {
    let geomFilter = null;
    if (coord) {
        const [lon, lat] = coord;
        geomFilter = `POINT(${lon} ${lat})`;
    }
    setParcelLayer(null, geomFilter);
}

function setParcelLayerVisibility(visible) {
    parcelLayer.setVisible(visible);
}

function setParcelLayer(attrFilter, geomFilter) {
    parcelLayer.setSource(null);

    if (!attrFilter && !geomFilter) {
        return
    }

    attrFilter = attrFilter || '';
    geomFilter = geomFilter || '';

    const url = 'https://api.vworld.kr/req/data';
    const key = 'AF9CD299-B057-3699-B38C-9C3C007947F2';
    const domain = 'https://map.epinet.kr';
    const data = `service=data&request=GetFeature&crs=EPSG:3857&data=LP_PA_CBND_BUBUN&key=${key}&domain=${domain}&attrFilter=${attrFilter}&geomFilter=${geomFilter}&format=json`;

    $.ajax({
        url: url,
        data: data,
        dataType: 'jsonp',
        async: false,
        success: function (data) {
            // console.log(data);
            const result = data.response.result;
            let layerSource = null;
            if (result) {
                layerSource = new ol.source.Vector({
                    features: (new ol.format.GeoJSON()).readFeatures(result.featureCollection)
                });
            }
            parcelLayer.setSource(layerSource);
        }
    })
}

function testFunc() {
    alert(new Date());
}

$btnDrawer.onclick = (e) => {
    if ($controller.classList.contains('visible')) {
        $btnDrawer.innerHTML = '&#9776;';
    } else {
        $btnDrawer.innerHTML = '&#x2715;';
    }
    $controller.classList.toggle('visible');
    $btnDrawer.classList.toggle('open');
}

$inputDate.onchange = function () {
    updateDataLayer();
}

$selectLayerInfo.onchange = function () {
    updateDataLayer();
}

$selectOpacity.onchange = function () {
    dataLayer.setOpacity(getOpacity());
}

$btnTest.onclick = function () {
    if (window.JavascriptChannel) {
        window.JavascriptChannel.postMessage(JSON.stringify(new Date()));
    }
}

olMap.on('moveend', function (e) {
    const coord = olMap.getView().getCenter();
    const zoom = olMap.getView().getZoom();

    if (zoom > 12) {
        setParcelLayerByCoord(coord);
    } else {
        setParcelLayerByCoord(null);
    }
});